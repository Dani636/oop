/*
 * Container.cpp
 *
 *  Created on: 22.06.2014
 *      Author: DANI
 */

#include "MediaContainer.h"

template <class T>
MediaContainer<T>::MediaContainer()
{
	this->lastItem = 0;
	this->ItemPoint = this->lastItem;

};

template <class T>
MediaContainer<T>::~MediaContainer()
{
	// TODO Auto-generated destructor stub
};

template <class T>
struct MediaContainer<T>::ListItem
{
		T& element;
		ListItem* nextItem;
};

template <class T>
bool MediaContainer<T>::add(T& element)
{
	ListItem* listItem;

	listItem->element = element;
	listItem->nextItem = this->lastItem;

	this->lastItem = listItem;

	return true;
};

template <class T>
bool MediaContainer<T>::remove()
{
	if(this->lastItem != 0)
	{
		ListItem* currentItem = this->ItemPoint;

		if(currentItem->nextItem != 0)
		{
			this->begin();

			//find predecessor
			while(currentItem != this->*ItemPoint->nextItem)
			{
				this->next();

				if(this->ItemPoint == 0)
				{
					return false;
				}

			}

			this->*ItemPoint->nextItem = currentItem->nextItem;

			delete *currentItem;

			return true;
		}
		else
		{
			delete this->*ItemPoint;
			this->ItemPoint = 0;
			return true;
		}
	}

	return false;

};

template <class T>
void MediaContainer<T>::begin()
{
	this->ItemPoint = this->lastItem;
};


template <class T>
void MediaContainer<T>::next()
{
	this->ItemPoint = this->*ItemPoint->nextItem;
};


template <class T>
T* MediaContainer<T>::getItem()
{
	return this->*ItemPoint->element;
};









