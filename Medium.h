/*
 * Medium.h

 *
 *  Created on: 06.04.2014
 *      Author: DANI
 */
#include <iostream>
#include <sstream>
#include <typeinfo>
#include <string>
#include <stdlib.h>
#include <iomanip>
#include <limits>
using namespace std;




#ifndef MEDIUM_H_
#define MEDIUM_H_

enum 	Status{entliehen, praesent};

class Medium
{

public:
			Medium(bool option);
	bool 	borrow();
	bool	returnIt();
	virtual string	toString();

	Status	isAvailable();
	int 	getSignature();

	template <class T>
	static void readNumber(T& number);
	static void readdouble(double& number);

protected:

	string	getType();
	string	getTitle();
	bool addMedia(char option);

	int 	signature;
	string 	type;
	string 	title;
	Status 	status;

};


#endif
