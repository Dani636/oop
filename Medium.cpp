

// Medium cpp

#include "medium.h"



Medium::Medium(bool option)
{
	if(option)
	{

		cout << "Bitte Mediandaten eingeben: Signatur und Titel";

		cout << "\nSignatur:";
		Medium::readNumber(this->signature);
		
		cout << "\nTitel:";
		getline(cin,this->title);
		cout<<endl<<endl;
	}

	status = praesent;
}

int Medium::getSignature()
{
	return this->signature;
}

string Medium::getType()
{
	return this->type;
}

string Medium::getTitle()
{
	return this->title;
}

Status Medium::isAvailable()
{
	return this->status;
}

/**
 * lends a medium of
 * returns false if the medium isn't available
 */
bool Medium::borrow()
{
	if(this->isAvailable())
	{
		this->status = entliehen;
		return true; // is now on loan
	}
	else
	{
		return false;
	}

}

bool Medium::returnIt()
{
	if(this->isAvailable())
	{
		return false;
	}
	else
	{
		this->status = praesent;
		return true;
	}
}

template <class T>
void Medium::readNumber(T& number){
        if (cin >> number){}
        else{
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                cout << "Ungueltige Eingabe." << endl;
				readNumber(number);
        }
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        return;
}

void Medium::readdouble(double& number){
        if (cin >> number){}
        else{
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                cout << "Ungueltige Eingabe." << endl;
                readdouble(number);
        }
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        return;
}

string Medium::toString()
{

	/*cout << endl << left << setw(10) << "Signatur" << setw(10)  <<  "Typ" << setw(10)
		 << "Titel"      << setw(10) << "Status"   << setw(10)  << endl;*/

	cout << right <<setw(10)<< this->getSignature() << setw(10);
	cout << left << "Medium ";
	cout <<setw(10)<< this->title.substr(0,8) << setw(10);

	if(this->isAvailable())
		cout << "vorh." << setw(10) ;
	else
		cout << "nicht vorh." << setw(10) ;

	cout << endl;

	return "Medium";
}
















