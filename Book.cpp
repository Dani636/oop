#include"Book.h"

Book::Book() : Medium(false)
{
	
		cout << "Bitte Mediandaten eingeben: Signatur Titel Seitenzahl";
		
		cout << "\nSignatur:";
		Medium::readNumber(this->signature);

		cout << "\nSeitenzahl:";
		Medium::readNumber(this->pages);

		cout << "\nTitel:";
		getline(cin,this->title);
		cout<<endl<<endl;
}

string Book::toString()
{
	/*cout << endl << left << setw(10) << "Signatur" << setw(10)  <<  "Typ" << setw(10)
		 << "Titel"      << setw(10) << "Status"   << setw(10) <<  "Seitenzahl: " << setw(10) <<  endl;*/

	cout << left <<setw(10)<<this->getSignature() << setw(10);
	cout <<"Buch" << setw(10);
	cout << this->getTitle().substr(0,8) << setw(10);

	if(this->isAvailable())
		cout << "vorh." << setw(10) ;
	else
		cout << "nicht vorh." << setw(10) ;

	cout << this->getPages() << endl;

	return "Medium";
}

int Book::getPages()
{
	return this->pages;
}
