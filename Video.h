/*
 * Video.h
 *
 *  Created on: 11.05.2014
 *      Author: DANI
 */
#include"Medium.h"
#ifndef VIDEO_H_
#define VIDEO_H_

using namespace std;

class Video :  Medium
{
private:
	double playingTime;

public:
	Video();
	string toString(void);

protected:
	double getPlayingTime();
};



#endif /* VIDEO_H_ */
