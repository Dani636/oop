/*
 * Book.h
 *
 *  Created on: 10.05.2014
 *      Author: DANI
 */
#include"Medium.h"
#ifndef BOOK_H_
#define BOOK_H_

using namespace std;

class Book : Medium
{

private:
	int pages;

public:
	
	Book();
	string toString();

protected:
	int getPages();


};



#endif /* BOOK_H_ */

