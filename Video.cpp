
#include "Video.h"

Video::Video() : Medium(false)
{
		cout << "Bitte Mediandaten eingeben: Signatur Titel Spielzeit";
		
		cout << "\nSignatur:";
		Medium::readNumber(this->signature);

		cout << "\nSpieldauer:";
		Medium::readdouble(this->playingTime);

		cout << "\nTitel:";
		getline(cin,this->title);
		cout<<endl<<endl;
}

string Video::toString()
{
	/*cout << endl << left << setw(10) << "Signatur" << setw(10)  <<  "Typ" << setw(10)
		 << "Titel"      << setw(10) << "Status"   << setw(10)  << "Spielzeit" << setw(10) << endl;*/

	cout << left <<setw(10)<< this->getSignature() << setw(10);
	cout <<setw(10)<<" Video ";
	cout <<setw(10)<< this->getTitle().substr(0,8) << setw(10);

	if(this->isAvailable())
		cout << "vorh." << setw(10) ;
	else
		cout << "nicht vorh." << setw(10) ;

	cout <<fixed<<setprecision(2)<<right<<setw(10)<<this->getPlayingTime() << endl;

	return "Medium";
}

double Video::getPlayingTime()
{
	return this->playingTime;
}


