/*
 * Container.h
 *
 *  Created on: 22.06.2014
 *      Author: DANI
 */


#ifndef CONTAINER_H_
#define CONTAINER_H_



template <class T>
class MediaContainer
{
public:
	MediaContainer();

	bool add(T& element);

	bool remove();
	void begin();
	void next();

	T* getItem();

	virtual ~MediaContainer();


private:

	struct ListItem;

	ListItem* lastItem;
	ListItem* ItemPoint;


};

#endif /* CONTAINER_H_ */
